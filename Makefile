all:

clean:

install:
	install -d $(DESTDIR)/usr/bin
	install -o root -g root -m 755 texify $(DESTDIR)/usr/bin/texify
	ln -s texify $(DESTDIR)/usr/bin/texifyabel
	ln -s texify $(DESTDIR)/usr/bin/texifyada
	ln -s texify $(DESTDIR)/usr/bin/texifyasm
	ln -s texify $(DESTDIR)/usr/bin/texifyaxiom
	ln -s texify $(DESTDIR)/usr/bin/texifyB
	ln -s texify $(DESTDIR)/usr/bin/texifybeta
	ln -s texify $(DESTDIR)/usr/bin/texifybison
	ln -s texify $(DESTDIR)/usr/bin/texifyc
	ln -s texify $(DESTDIR)/usr/bin/texifyc++
	ln -s texify $(DESTDIR)/usr/bin/texifyidl
	ln -s texify $(DESTDIR)/usr/bin/texifyjava
	ln -s texify $(DESTDIR)/usr/bin/texifylex
	ln -s texify $(DESTDIR)/usr/bin/texifylisp
	ln -s texify $(DESTDIR)/usr/bin/texifylogla
	ln -s texify $(DESTDIR)/usr/bin/texifymatlab
	ln -s texify $(DESTDIR)/usr/bin/texifyml
	ln -s texify $(DESTDIR)/usr/bin/texifyperl
	ln -s texify $(DESTDIR)/usr/bin/texifypromela
	ln -s texify $(DESTDIR)/usr/bin/texifypython
	ln -s texify $(DESTDIR)/usr/bin/texifyruby
	ln -s texify $(DESTDIR)/usr/bin/texifyscheme
	ln -s texify $(DESTDIR)/usr/bin/texifysim
	ln -s texify $(DESTDIR)/usr/bin/texifysql
	ln -s texify $(DESTDIR)/usr/bin/texifyvhdl
	install -d $(DESTDIR)/usr/share/man/man1
	install -o root -g root -m 644 texify.1 $(DESTDIR)/usr/share/man/man1/texify.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyabel.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyada.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyasm.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyaxiom.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyB.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifybeta.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifybison.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyc.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyc++.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyidl.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyjava.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifylex.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifylisp.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifylogla.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifymatlab.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyml.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyperl.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifypromela.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifypython.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyruby.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyscheme.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifysim.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifysql.1
	ln -s texify.1 $(DESTDIR)/usr/share/man/man1/texifyvhdl.1
